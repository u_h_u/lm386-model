Model of LM386 circuit

Copyright (C) 2018 Uro� Hudomalj

The files in this repository are free to use: you can redistribute them and/or modify
them under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or any later version.

This files are distributed in the hope that they will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

The following files are contained in this repository (described in Slovene).
Datoteke za modeliranje vezja LM386:

lm386.inc : moj razvit model vezja LM386
lm386_a.inc : prosto dostopen model vezja LM386, pridobljen iz https://groups.google.com/forum/#!topic/sci.electronics/Ssv3p_PDaw8
lm386_b.inc : prosto dostopen model vezja LM386, pridobljen iz https://easyeda.com/example/Tesseract_Guitar_Practice_Amp_simulation_files-H0ca8IFDB

lm386_test_sim.cir : vsebuje izvedene simulacije (meritve) mojega razvitega modela
lm386_a_test_sim.cir : vsebuje izvedene simulacije (meritve) modela lm386_a
lm386_b_test_sim.cir : vsebuje izvedene simulacije (meritve) modela lm386_b
*V teh treh datotekah so vse simulacije enake, vsebujejo le razlicne modele
**Same simulacije so opisane v "Modeliranje_integriranega_vezja_LM386.pdf"

lm386_test_op.cir : datoteka, namenjena izvajanju optimizacije mojega razvitega modela

optimizacija.pog : krovna datoteka procesa optimizacije - vsebuje uporabljene analize, izvedene meritve in definicijo optimizacijskega taska (imenovan eval)
eval.sqlite : vsebuje potek optimizacije za pridobitev koncnih parametrov za moj razvit model