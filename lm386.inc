.subckt lm386 (1 2 3 4 5 6 7 8)

 *upori
 r1 (6  7) r=15k
 r2 (7 13) r=15k
 r3 (13 8) r=150
 r4 (8  1) r=1.35k
 r5 (1  5) r=15k
 r6 (3  4) r=50k
 r7 (2  4) r=50k
 
 *tokovni vir
 i0 (6  9) dc={i0_dc}
 
 *diode
 d1 (9 91) dmod area={da}
 d2 (91 10) dmod area={da}
 .model dmod D (is={is}, n={n}, rs={rs}, vj={vj})
 
 *tranzistorji
 q1 (6  9   5) mnpn area={m1} m=16
 q2 (5  41  4) mnpn area={m2} m=8
 q3 (41 10  5) mpnp area={m3} 
 q4 (10 11  4) mnpn area={m4}
 
 q5 (12 21 13) mpnp area={m5}
 q6 (11 31  1) mpnp area={m5}
 q7 (4  2  21) mpnp area={m7}
 q8 (4  3  31) mpnp area={m7}
 
 q9 (12 12  4) mnpn area={m9}
 q10 (11 12  4) mnpn area={m10}

.model mnpn NPN(IS=0.15E-15 VAF=20 BF=800 IKF=0.02
+ XTB=1.5 BR=4 CJC={cjc} CJE=18e-12 VJC={vjc} ITF=1 VTF=2 XTF=3 )
 
.model mpnp PNP(IS=0.15E-15 VAF=20 BF=200 IKF=0.02
+ XTB=1.5 BR=4 CJC={cjc} CJE=18e-12 VJC={vjc} ITF=1 VTF=2 XTF=3 )
 
.ends